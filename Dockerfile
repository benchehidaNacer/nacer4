FROM node:latest

COPY  . .

EXPOSE 8081:8081

CMD ["node", "server.js"]
